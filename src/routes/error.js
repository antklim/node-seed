/**
 * Expose `ErrorHandler`.
 */
exports = module.exports = ErrorHandler;

function ErrorHandler(handler, app) {
	"use strict";

	this.errorPage = function(err, req, res, next) {
		handler.emit('error', err);

		var title = app.get('title') + ": Internal Error";

		res.status(500);
		res.render('error_template', { title: title, errorMesg: err.message, errorStack: err.stack });
	};
}