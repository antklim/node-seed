/**
 * Module dependencies.
 */

/**
 * Expose `ContentHandler`.
 */
exports = module.exports = ContentHandler;

function ContentHandler(handler, app) {
	"use strict";

	this.mainPage = function(req, res, next) {
		res.status(200);
		return res.render('main_template', { title: app.get('title'), time: new Date().getTime() });
	};

	this.pathNotFound = function (req, res, next) {
		return res.status(404).send("Sorry, path not found.");
	};
}