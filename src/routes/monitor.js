/**
 * Module dependencies.
 */

/**
 * Expose `MonitorHandler`.
 */
exports = module.exports = MonitorHandler;

function MonitorHandler(handler, app, monitor) {
	"use strict";

	this.mainPage = function(req, res, next) {
		res.status(200);
		return res.render('monitor_template', { title: app.get('title'), started: monitor.started.format() });
	};

	this.pathNotFound = function (req, res, next) {
		return res.status(404).send("Sorry, path not found.");
	};
}