/**
 * Module dependencies.
 */
var path = require('path'),
	auth = require('basic-auth'),
	AuthController = require('../controllers/auth');

/**
 * Expose `AuthHandler`.
 */
exports = module.exports = AuthHandler;

function AuthHandler(handler, app) {
	"use strict";

	var ac = new AuthController();

	var unauthorized = function(res) {
		return res.status(401).end();
	};

	this.adminAuth = function(req, res, next) {
		res.set({
			'WWW-Authenticate': "Basic realm=Authorization Required",
			'Content-Type':     "text/html",
			'charset':          "utf-8"
		});

		var user = auth(req);

		if (!user || !user.name || !user.pass) {
			return unauthorized(res);
		}

		ac.adminAuth(user.name, user.pass, function(err, isAuth){
			if (err) {
				handler.emit('error', err, "adminAuth");
				return unauthorized(res);
			}

			if (!isAuth) {
				return unauthorized(res);
			}
			return next();
		});
	};

	this.monitorAuth = function(req, res, next) {
		res.set({
			'WWW-Authenticate': "Basic realm=Authorization Required",
			'Content-Type':     "text/html",
			'charset':          "utf-8"
		});

		var user = auth(req);
		
		if (!user || !user.name || !user.pass) {
			return unauthorized(res);
		}

		ac.monitorAuth(user.name, user.pass, function(err, isAuth){
			if (err) {
				handler.emit('error', err, "monitorAuth");
				return unauthorized(res);
			}

			if (!isAuth) {
				return unauthorized(res);
			}
			return next();
		});
	};
}