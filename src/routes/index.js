/**
 * Module dependencies.
 */

var AuthHandler    = require('./auth'),
	ContentHandler = require('./content'),
	AdminHandler   = require('./admin'),
	MonitorHandler = require('./monitor'),
	ErrorHandler   = require('./error');

module.exports = exports = function (handler, app, monitor) {

	var authHandler    = new AuthHandler(handler, app),
		contentHandler = new ContentHandler(handler, app),
		adminHandler   = new AdminHandler(handler, app),
		monitorHandler = new MonitorHandler(handler, app, monitor),
		errorHandler   = new ErrorHandler(handler, app);

	// Main page
	app.get('/',                contentHandler.mainPage);
	app.get('/path_not_found',  contentHandler.pathNotFound);
	app.post('/',               contentHandler.mainPage);
	app.post('/path_not_found', contentHandler.pathNotFound);

	// Monitoring page
	app.get('/admin',                 authHandler.adminAuth, adminHandler.mainPage);
	app.get('/admin/path_not_found',  adminHandler.pathNotFound);
	app.post('/admin',                authHandler.adminAuth, adminHandler.mainPage);
	app.post('/admin/path_not_found', adminHandler.pathNotFound);

	// Monitoring page
	app.get('/monitor',                 authHandler.monitorAuth, monitorHandler.mainPage);
	app.get('/monitor/path_not_found',  monitorHandler.pathNotFound);
	app.post('/monitor',                authHandler.monitorAuth, monitorHandler.mainPage);
	app.post('/monitor/path_not_found', monitorHandler.pathNotFound);

	// Error handling middleware
	app.use(errorHandler.errorPage);
};