var _       = require('lodash'),
	fs      = require('fs'),
	path    = require('path'),
	winston = require('winston');

var addLogger = function(config, category) {
	var logMode = config.logMode || "test",
		logDir  = config.logDir  || process.cwd(),
		logFile = path.join(logDir, category + ".log");

	if (logMode === "test") {
		winston.loggers.add(category, {
			transports: [
				new (winston.transports.Console)({colorize: true})
			]
		});
	} else if (logFile) {
		winston.loggers.add(category, {
			transports: [
				new (winston.transports.File)({
					filename: logFile,
					maxsize: 20971520,	// MAX size of log file in bytes (current value is 20MB)
					json: true,			// messages will be logged as JSON
					colorize: true		// colorize output
				})
			]
		});
	}

	return winston.loggers.get(category);
};

var loadConfig = function(filePath) {
	var config;

	if (fs.existsSync(filePath)) {
		try {
			config = require(filePath);
		} catch (e) {
			return e;
		}
	} else {
		return new Error("Config file not found.");
	}

	return config;
};

var isObject = function(a) {
	return _.isObject(a) && !_.isArray(a) && !_.isFunction(a);
};

exports.addLogger  = addLogger;
exports.loadConfig = loadConfig;
exports.isObject   = isObject;