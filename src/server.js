/**
 * Module dependencies.
 */
var _          = require('lodash'),
	path       = require('path'),
	util       = require('util'),
	events     = require('events'),
	http       = require('http'),
	https      = require('https'),
	express    = require('express'),
	bodyParser = require('body-parser'),
	moment     = require('moment'),
	uuid       = require('node-uuid'),

	// Template engine, could be changed to other
	cons       = require('consolidate'),

	// Custom modules *********************************************************
	// Local modules
	routes = require('./routes'),

	Utils  = require('./modules').Utils;

var LOGGER = { 'log': console.log };

/**
 * Expose `Server`.
 */
exports = module.exports = Server;
util.inherits(Server, events.EventEmitter);

function Server(config) {
	"use strict";
	events.EventEmitter.call(this);

	var self     = this;
	var self_ID  = uuid.v1();
	var self_app = express();
	var self_name;
	var self_server;
	var self_config;
	var self_monitor;

	this.init = function() {
		// Load config
		self_config = Utils.isObject(config) ? _.cloneDeep(config) : {};
		self_name   = self_config.title || "MyServer";

		// Add logger
		LOGGER = Utils.addLogger(self_config, self_name);

		// Validate config and run server
		if (!isConfigValid(self_config)) {
			return new Error("Server configuration is not valid.");
		}

		initServer(self_config.SERVER);
		initSelfEvents();
		return self;
	};

	this.stop = function(callback) {
		callback = (_.isFunction(callback)) ? callback : function() {};

		if (!self_server) { return callback(); }

		try {
			self_server.close(callback);
		} catch (e) {
			LOGGER.log('error', self_name, e);
			return callback(e);
		}
	};

	// Initiators *************************************************************
	var initServer = function(config) {
		self_monitor = {started: moment.utc()}; // Store start timestamp in UTC

		self_app.set('title', self_name);

		// Register templating engine
		self_app.engine('html', cons.swig);
		self_app.set('view engine', 'html');
		self_app.set('views', path.join(__dirname, 'views/pages'));

		// Express middleware to populate 'req.body' so we can access POST variables
		self_app.use(bodyParser.json());
		self_app.use(bodyParser.urlencoded({ extended: false }));

		// Register available routes
		routes(self, self_app, self_monitor);

		switch(config.protocol) {
			case "https":
				self_server = https.createServer({}, self_app).listen(config.port || 443);
				break;
			default:
				self_server = http.createServer(self_app).listen(config.port || 80);
				break;
		}

		self_server.on('listening',   function() { onHTTPListening(); });	// HTTP/HTTPS Server listening
		self_server.on('clientError', function(exception, socket) {			// Client error handler on HTTP/HTTPS Server
			onHTTPClientError(exception, socket);
		});
	};

	var initSelfEvents = function() {
		self.on('error', onSelfError);
	};

	// Event listeners ********************************************************
	var onHTTPListening = function() {
		LOGGER.log('info', "Listening " + self_name + " on PORT: " + self_config.SERVER.port);
	};

	var onHTTPClientError = function(exception, socket) {
		LOGGER.log('error', self_name + ":onClientError.", exception);
	};

	var onSelfError = function(err, note) {
		if (note) { LOGGER.log('error', note, err); }
		else { LOGGER.log('error', err); }
	};

	// Validators *************************************************************
	var isConfigValid = function(config) {
		return Utils.isObject(config.SERVER);
	};
}