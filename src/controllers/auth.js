/**
 * This is template for authentification
 */

/**
 * Module dependencies.
 */

/**
 * Expose `AuthController`.
 */
exports = module.exports = AuthController;

function AuthController() {
	"use strict";

	this.adminAuth = function(user, pswd, callback) {
		return callback(null, true);
	};

	this.monitorAuth = function(user, pswd, callback) {
		return callback(null, true);
	};
}