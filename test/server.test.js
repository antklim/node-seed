var expect = require('expect.js'),
	http   = require('http'),
	q      = require('q'),
	url    = require('url'),
	Server = require('../');

var config = require('./config/test.config');

var makeRequest = function(options) {
	var deferred = q.defer();
	var promise  = deferred.promise;

	var req = http.request(options, function(response) {
		if (parseInt(response.statusCode, 10) !== 200) {
			return deferred.reject(new Error("Error code."));
		}

		response.on('data', function (chunk) {});
		response.on('end', function() {
			deferred.resolve();
		});
	});

	req.on('error', function(err) {
		deferred.reject(err);
	});

	req.setTimeout(800, function() {
		req.abort();
		deferred.reject(new Error("Request time."));
	});

	req.end();
	return promise;
};

var getRequest = function(path) {
	var options = {
		hostname: config.SERVER.hostname,
		port:     config.SERVER.port
	};

	if (path) { options.path = path; }

	return makeRequest(options);
};

var postRequest = function(path) {
	var options = {
		hostname: config.SERVER.hostname,
		port:     config.SERVER.port,
		method:   "POST"
	};

	if (path) { options.path = path; }

	return makeRequest(options);
};

describe('Server', function () {
	it('should return error for incorrect config', function() {
		var server = new Server().init();

		expect(server).to.be.an(Error);
		expect(server.message).to.be.eql("Server configuration is not valid.");
	});

	it('should run server', function(done) {
		var server = new Server(config).init();
		expect(server).not.to.be.an(Error);

		// Testing requests
		var proms = [];

		proms.push(
			getRequest(),
			postRequest()
			);

		q.all(proms)
			.then(function() {
				server.stop(function(err) {
					expect(err).not.to.be.ok();
					done();
				});
			})
			.catch(function(err) {
				expect().fail("It should not return error.");
			});
	});
});