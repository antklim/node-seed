var path   = require('path'),
	Server = require('./');

// Command line arguments parser
var nopt       = require('nopt'),
	knownOpts  = { "Mode": String },
	shortHands = { "ModeDef": [ "--Mode", "dev" ] },
	argv       = nopt(knownOpts, shortHands, process.argv, 2);

var mode   = argv.Mode || "dummy";
var config = require(path.join(__dirname, 'test/config', mode + ".config"));
var server = new Server(config).init();