node-seed
=========

[![Codacy Badge](https://www.codacy.com/project/badge/f5eb27ce082a4a158e17df77938e4d4e)](https://www.codacy.com/app/anton-klimenko/node-seed)

####How to start
1. `#git clone git@github.com:antklim/node-seed.git [LOCATION]` - clone source code to `LOCATION`.
2. `#cd [LOCATION]`
3. `#npm install` - install all dependecies.
4. `#make test` - run all tests (optional).
5. `#node example.js --ModeDef` - run server on localhost:8888. All settings are in `test/config`.

_Note: only `/`, `/admin`, `/monitor` paths available._